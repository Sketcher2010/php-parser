<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190626212648 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE vk_group (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vk_id INTEGER NOT NULL, members_count INTEGER NOT NULL, last_offset INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE vk_user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vk_id INTEGER NOT NULL, city_id INTEGER DEFAULT NULL, city_name VARCHAR(255) DEFAULT NULL, has_mobile BOOLEAN DEFAULT NULL, find_in INTEGER DEFAULT NULL, is_friends_parsed BOOLEAN NOT NULL)');
        $this->addSql('DROP TABLE "group"');
        $this->addSql('DROP TABLE user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE "group" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vk_id INTEGER NOT NULL, members_count INTEGER NOT NULL, last_offset INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vk_id INTEGER NOT NULL, city_id INTEGER DEFAULT NULL, city_name VARCHAR(255) DEFAULT NULL COLLATE BINARY, has_mobile BOOLEAN DEFAULT NULL, find_in INTEGER DEFAULT NULL, is_friends_parsed BOOLEAN NOT NULL)');
        $this->addSql('DROP TABLE vk_group');
        $this->addSql('DROP TABLE vk_user');
    }
}
