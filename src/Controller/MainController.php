<?php

namespace App\Controller;

use App\Entity\VkGroup;
use App\Entity\VkUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiParamGroupIdException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class MainController extends AbstractController
{
    private $client_id = 5349625;
    private $client_secret = 'jwueFp1ofz3wQcwJgrQ1';


    private $access_token = 'ead9adc07a57a230a9d0aafd1fd20a2e573c1bfe65677a424d623afd2e52c4a03facd905905002c13d512';

    /**
     * @Route("/", name="main")
     */
    public function index()
    {

        $oauth = new VKOAuth();
        $client_id = 5349625;
        $redirect_uri = 'http://localhost:8888/vk';
        $display = VKOAuthDisplay::PAGE;
        $scope = array(VKOAuthUserScope::WALL, VKOAuthUserScope::GROUPS);

        $browser_url = $oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $client_id, $redirect_uri, $display, $scope);
        return new Response("<a href='$browser_url'>click</a>");

//        return $this->render('main/index.html.twig', [
//            'controller_name' => 'MainController',
//        ]);
    }

    /**
     * @Route("/vk", name="vk")
     * @param Request $request
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    public function vkAction(Request $request)
    {
        $oauth = new VKOAuth();
        $client_id = 5349625;

        $client_secret = 'jwueFp1ofz3wQcwJgrQ1';
        $redirect_uri = 'http://localhost:8888/vk';
        $code = $request->query->get('code');

        $response = $oauth->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
        $access_token = $response['access_token'];
        dd($access_token);
    }

    /**
     * @Route("/parse/groups", name="groups")
     */
    public function parseGroupsAction()
    {
        $vk = new VKApiClient();
        $manager = $this->getDoctrine()->getManager();
        $groups = [
            "aciworldwideru",
            "fix_company",
            "jetbrains",
            "snowbars_siberia",
            "amostart_info",
            "ceptechnology",
            "club17061846",
            "imagine_agency",
            "blogman.team",
            "abvsoft",
            "gooodnet",
            "itt_technology",
            "sharksoftware",
            "unisitegroup",
            "xmemo",
            "utelksp",
            "megaorion32",
            "simtechru",
            "avalon03",
            "yode_pro",
            "redwoodit",
            "vasgacom",
            "web_boomerangoo",
            "cybergen.city",
            "klaster24",
            "vrnet",
            "flashformat",
            "club168683450",
            "libertasgroupkz",
            "mining_tmn",
            "itpeak",
            "tda_groups",
            "pcm_official",
            "hrbit",
            "massmedia_group",
            "vinstrok"
        ];

        $res = $vk->groups()->getById($this->access_token, [
            'group_ids' => $groups,
            'fields' => 'members_count'
        ]);

        foreach ($res as $gr) {
            $group = new VkGroup();
            $group->setVkId($gr["id"]);
            $group->setMembersCount($gr["members_count"]);
            $group->setLastOffset(0);
            $manager->persist($group);
            $manager->flush();
        }
        return new Response('success');
    }

//    /**
//     * @Route("/parse", name="parse")
//     */
//    public function parseAction()
//    {
//        $manager = $this->getDoctrine()->getManager();
//        $rep = $this->getDoctrine()->getRepository(VkGroup::class);
//        $vk = new VKApiClient();
//        $groups = $rep->findAll();
//        $totalMembersParsed = 0;
//
//        foreach ($groups as $group) {
////            dd($group->getLastOffset(), $group->getMembersCount());
//            while ($group->getLastOffset() < $group->getMembersCount()) {
//                try {
//                    $response = $vk->groups()->getMembers($this->access_token, array(
//                        'group_id' => $group->getVkId(),
//                        'count' => '1000',
//                        'offset' => $group->getLastOffset(),
//                        'fields' => array('city', 'bdate', 'has_mobile', 'contacts', 'connections', 'common_count'),
//                    ));
//                } catch (VKApiParamGroupIdException $e) {
//                    dd($e);
//                } catch (VKApiException $e) {
//                    dd($e);
//                } catch (VKClientException $e) {
//                    dd($e);
//                }
//
//
//                foreach ($response["items"] as $u) {
//                    if (!empty($u["has_mobile"])) {
//
//                        $user = new VkUser();
//                        $user->setVkId($u['id']);
//                        if (isset($u['city'])) {
//                            $user->setCityId($u['city']['id']);
//                            $user->setCityName($u['city']['title']);
//                        }
//                        $user->setHasMobile($u["has_mobile"]);
//                        $user->setFindIn($group->getVkId());
//                        $user->setIsFriendsParsed(false);
//                        $manager->persist($user);
//                        $manager->flush();
//
//                    }
//                }
//                $group->setLastOffset($group->getLastOffset() + 1000);
//                $manager->persist($group);
//                $manager->flush();
//                $totalMembersParsed += count($response["items"]);
//            }
//        }
//
//        return new Response("Success! Total parsed: $totalMembersParsed users from groups.");
//
//    }

    /**
     * @Route("/parse", name="parse")
     */
    public function parseAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $rep = $this->getDoctrine()->getRepository(VkGroup::class);
        $vk = new VKApiClient();
        $totalMembersParsed = 0;

        try {
            $response = $vk->friends()->get($this->access_token, array(
                'user_id' => '299516526',
                'count' => '5000',
                'offset' => "0",
                'fields' => array('city', 'bdate', 'has_mobile'),
            ));
            dd($response);
        } catch (VKApiException $e) {
            dd($e);
        } catch (VKClientException $e) {
            dd($e);
        }

    }
}
