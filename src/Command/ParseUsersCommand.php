<?php

namespace App\Command;

use App\Entity\VkGroup;
use App\Entity\VkUser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiParamGroupIdException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class ParseUsersCommand extends Command
{
// the name of the command (the part after "bin/console")
    protected static $defaultName = 'parser:users';

    private $client_id = 5349625;
    private $client_secret = 'jwueFp1ofz3wQcwJgrQ1';


    private $access_token = 'ead9adc07a57a230a9d0aafd1fd20a2e573c1bfe65677a424d623afd2e52c4a03facd905905002c13d512';
    private $container;
    private $doctrine;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->doctrine->getManager();
        $rep = $this->doctrine->getRepository(VkGroup::class);
        $vk = new VKApiClient();
        $groups = $rep->findAll();
        $totalMembersParsed = 0;

        $output->writeln([
            "Начинаем парсинг " . count($groups) . " сообществ",
            "==================",
            ''
        ]);

        foreach ($groups as $group) {
            if ($group->getLastOffset() >= $group->getMembersCount()) {
                continue;
            }
            $now = 0;
            $output->writeln('Работаем с группой ' . $group->getVkId());
            $total = $group->getMembersCount();
            $output->writeln('');
            $section1 = $output->section();
            while ($group->getLastOffset() < $group->getMembersCount()) {
                try {
                    $response = $vk->groups()->getMembers($this->access_token, array(
                        'group_id' => $group->getVkId(),
                        'count' => '1000',
                        'offset' => $group->getLastOffset(),
                        'fields' => array('city', 'bdate', 'has_mobile', 'contacts', 'connections', 'common_count'),
                    ));
                } catch (VKApiParamGroupIdException $e) {
                    dd($e);
                } catch (VKApiException $e) {
                    dd($e);
                } catch (VKClientException $e) {
                    dd($e);
                }


                foreach ($response["items"] as $u) {
                    if (!empty($u["has_mobile"])) {
                        $user = new VkUser();
                        $user->setVkId($u['id']);
                        if (isset($u['city'])) {
                            $user->setCityId($u['city']['id']);
                            $user->setCityName($u['city']['title']);
                        }
                        $user->setHasMobile($u["has_mobile"]);
                        $user->setFindIn($group->getVkId());
                        $user->setIsFriendsParsed(false);
                        $manager->persist($user);
                        $manager->flush();

                        $now++;

                        $percents = ($now / $total) * 100;

                        $section1->overwrite("Парсинг пользователей: $now / $total; " . round($percents, 2) . "%");
                    }
                }
                $group->setLastOffset($group->getLastOffset() + 1000);
                $manager->persist($group);
                $manager->flush();
                $totalMembersParsed += count($response["items"]);
                $output->writeln([
                    "Все супер, собрали людей!",
                    "Потеряно " . ($total - $now) . " профелей по разным причинам",
                    '=========================================================',
                    ''
                ]);
            }
        }
        $output->writeln("FINISH!!! Собрано $totalMembersParsed пользователей!");
    }
}