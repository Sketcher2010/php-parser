<?php

namespace App\Command;

use App\Entity\Lead;
use App\Entity\VkGroup;
use App\Entity\VkUser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiParamGroupIdException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;


class FriendsToFileParserCommand extends Command
{
    protected static $defaultName = 'parser:friends';

    private $client_id = 5349625;
    private $client_secret = 'jwueFp1ofz3wQcwJgrQ1';


    private $access_token = 'ead9adc07a57a230a9d0aafd1fd20a2e573c1bfe65677a424d623afd2e52c4a03facd905905002c13d512';
    private $container;
    private $doctrine;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            "Проверяем наличие необходимой директории"
        ]);
        $filesystem = new Filesystem();
        if (!$filesystem->exists('~/project/php-parser/var/parser')) {
            $output->writeln([
                "Директории не существует, создаем!"
            ]);
            $filesystem->mkdir('~/project/php-parser/var/parser');
        }
        $fileName = 'var/parser/pars' . rand(0, 10000) . '.txt';
        $output->writeln([
            "Все ок, идем дальше. Создаем файл с именем $fileName!"
        ]);
        $filesystem->touch($fileName);
        $output->writeln([
            "Файл создали!",
            ""
        ]);

        $manager = $this->doctrine->getManager();
        $rep = $this->doctrine->getRepository(VkUser::class);
        $vk = new VKApiClient();
        $users = $rep->findAll();
        $totalFriendsParsed = 0;
        $totalUsersPassed = 0;
        $totalUsers = count($users);

        $output->writeln([
            "Начинаем парсинг друзей у " . count($users) . " пользователей",
            "==================",
            ''
        ]);

        $errors = $output->section();
        $globalUsers = $output->section();
        $currentUser = $output->section();

        foreach ($users as $user) {
//            if ($user->getIsFriendsParsed()) {
//                continue;
//            }
            $totalUsersPassed++;
            $globalPercents = round(($totalUsersPassed / $totalUsers) * 100, 2);
            $globalUsers->overwrite('Текущий пользователь: ' . $user->getVkId() . "; $totalUsersPassed/$totalUsers ($globalPercents %)");

            $tries = 0;
            $isSuccess = false;
            while (!$isSuccess && $tries < 10) {
                try {
                    $response = $vk->friends()->get($this->access_token, array(
                        'user_id' => $user->getVkId(),
                        'count' => '5000',
                        'offset' => '0',
                        'fields' => array('city', 'bdate', 'has_mobile'),
                    ));
                    $isSuccess = true;
                } catch (VKApiException $e) {
                    if ($e->getErrorCode() == 6) {
                        $tries++;
                        $isSuccess = false;
                        sleep(1);
                    } else {
                        $isSuccess = true;
                        $errors->writeln("User #" . $user->getVkId() . " not parserd! Reason: " . $e->getErrorMessage());
                        continue 2;
                    }
                    continue;
                } catch (VKClientException $e) {
                    if ($e->getErrorCode() == 6) {
                        $tries++;
                        $isSuccess = false;
                        sleep(1);
                    } else {
                        $isSuccess = true;
                        $errors->writeln("User #" . $user->getVkId() . " not parserd! Reason: " . $e->getErrorMessage());
                        continue 2;
                    }
                    continue;
                }
                $friendsList = $response['items'];
                if ((int)$response["count"] > 5000) {
                    try {
                        sleep(1);
                        $response = $vk->friends()->get($this->access_token, array(
                            'user_id' => $user->getVkId(),
                            'count' => '5000',
                            'offset' => '5000',
                            'fields' => array('city', 'bdate', 'has_mobile'),
                        ));
                        $isSuccess = true;
                    } catch (VKApiException $e) {
                        if ($e->getErrorCode() == 6) {
                            $tries++;
                            $isSuccess = false;
                            sleep(1);
                        } else {
                            $isSuccess = true;
                            $errors->writeln("User #" . $user->getVkId() . " not parserd! Reason: " . $e->getErrorMessage());
                            continue 2;
                        }
                        continue;
                    } catch (VKClientException $e) {
                        if ($e->getErrorCode() == 6) {
                            $tries++;
                            $isSuccess = false;
                            sleep(1);
                        } else {
                            $isSuccess = true;
                            $errors->writeln("User #" . $user->getVkId() . " not parserd! Reason: " . $e->getErrorMessage());
                            continue 2;
                        }
                        continue;
                    }
                    $friendsList = array_merge($friendsList, $response['items']);
                }
            }
            if($tries > 9) {
                $errors->writeln("User #" . $user->getVkId() . " not parserd! Reason: Can't call VK API SERVER");
                continue;
            }
            $currentFriendsPassed = 0;
            $totalFriends = count($friendsList);
            foreach ($friendsList as $friend) {
                $currentFriendsPassed++;
                if (!empty($friend["has_mobile"])) {
                    $currentPercents = round(($currentFriendsPassed / $totalFriends) * 100, 2);

//                    $lead = new Lead();
//                    $lead->setVkId($friend["id"]);
//                    if (isset($friend['city'])) {
//                        $lead->setCityId($friend['city']['id']);
//                        $lead->setCityName($friend['city']['title']);
//                    }
//                    $lead->setHasMobile($friend["has_mobile"]);
//                    $lead->setFindInUserId($user->getVkId());
//                    $manager->persist($lead);
//                    $manager->flush();

                    $filesystem->appendToFile($fileName, $friend["id"] . "\n");
                    if ($currentFriendsPassed % 100 == 0) {
                        $currentUser->overwrite("Собираем друзей; Статистика: $currentFriendsPassed/$totalFriends ($currentPercents %)");
                    }
                }
            }

            $totalFriendsParsed += $totalFriends;

//            $user->setIsFriendsParsed(true);
//            $manager->persist($user);
//            $manager->flush();
        }

        $output->writeln("FINISH!!! Собрано $totalFriendsParsed пользователей!");
    }
}