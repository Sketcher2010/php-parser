<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 */
class VkGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vk_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $membersCount;

    /**
     * @ORM\Column(type="integer")
     */
    private $lastOffset;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVkId(): ?int
    {
        return $this->vk_id;
    }

    public function setVkId(int $vk_id): self
    {
        $this->vk_id = $vk_id;

        return $this;
    }

    public function getMembersCount(): ?int
    {
        return $this->membersCount;
    }

    public function setMembersCount(int $membersCount): self
    {
        $this->membersCount = $membersCount;

        return $this;
    }

    public function getLastOffset(): ?int
    {
        return $this->lastOffset;
    }

    public function setLastOffset(int $lastOffset): self
    {
        $this->lastOffset = $lastOffset;

        return $this;
    }
}
