<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeadRepository")
 */
class Lead
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vkId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $has_mobile;

    /**
     * @ORM\Column(type="integer")
     */
    private $findInUserId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $city_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVkId(): ?int
    {
        return $this->vkId;
    }

    public function setVkId(int $vkId): self
    {
        $this->vkId = $vkId;

        return $this;
    }

    public function getHasMobile(): ?bool
    {
        return $this->has_mobile;
    }

    public function setHasMobile(bool $has_mobile): self
    {
        $this->has_mobile = $has_mobile;

        return $this;
    }

    public function getFindInUserId(): ?int
    {
        return $this->findInUserId;
    }

    public function setFindInUserId(int $findInUserId): self
    {
        $this->findInUserId = $findInUserId;

        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->city_id;
    }

    public function setCityId(?int $city_id): self
    {
        $this->city_id = $city_id;

        return $this;
    }

    public function getCityName(): ?string
    {
        return $this->city_name;
    }

    public function setCityName(?string $city_name): self
    {
        $this->city_name = $city_name;

        return $this;
    }
}
