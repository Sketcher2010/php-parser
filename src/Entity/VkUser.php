<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class VkUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vk_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $city_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_mobile;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $find_in;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFriendsParsed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVkId(): ?int
    {
        return $this->vk_id;
    }

    public function setVkId(int $vk_id): self
    {
        $this->vk_id = $vk_id;

        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->city_id;
    }

    public function setCityId(?int $city_id): self
    {
        $this->city_id = $city_id;

        return $this;
    }

    public function getCityName(): ?string
    {
        return $this->city_name;
    }

    public function setCityName(?string $city_name): self
    {
        $this->city_name = $city_name;

        return $this;
    }

    public function getHasMobile(): ?bool
    {
        return $this->has_mobile;
    }

    public function setHasMobile(?bool $has_mobile): self
    {
        $this->has_mobile = $has_mobile;

        return $this;
    }

    public function getFindIn(): ?int
    {
        return $this->find_in;
    }

    public function setFindIn(?int $find_in): self
    {
        $this->find_in = $find_in;

        return $this;
    }

    public function getIsFriendsParsed(): ?bool
    {
        return $this->isFriendsParsed;
    }

    public function setIsFriendsParsed(bool $isFriendsParsed): self
    {
        $this->isFriendsParsed = $isFriendsParsed;

        return $this;
    }
}
